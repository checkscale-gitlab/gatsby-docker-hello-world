FROM node:10.15-alpine

# Exposing Gatsby port and VSCode debug ports
EXPOSE 8000 9929 9230

RUN apk update && apk add git && rm -rf /var/cache/apk/*
RUN npm install --global gatsby-cli@2.6.5 yarn

RUN mkdir -p /site
WORKDIR /site
VOLUME /site

COPY ./entry.sh /
RUN chmod +x /entry.sh
ENTRYPOINT ["/entry.sh"]
